export interface RepositoryNodeType {
    name: string,
    projectsUrl: string,
    forkCount: number,
    stargazerCount: number,
    id: string,
}

export interface PageInfoType {
    startCursor: string,
    endCursor: string,
}

export interface QueryResultType {
    search: SearchResultType,
}

export interface SearchResultType {
    edges: RepositoryListType[],
    pageInfo: PageInfoType,
    repositoryCount: number,
}

export interface RepositoryListType {
    node: RepositoryNodeType,
}


