import React, {useRef} from 'react';
import { gql, useQuery } from '@apollo/client';
import Loading from "../Loading/Loading";
import OnError from "../OnError/OnError";
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import RepositoryList from "../RepositoryList/RepositoryList";
import {RepositoryListType, QueryResultType} from '../../Types/Interface';

const GET_REPOSITORY_LIST = gql`
  query GetRepositoryList($query: String!, $type: SearchType!, $first: Int!, $after: String) {
  search(query: $query, type: $type, first: $first, after: $after ) {
  repositoryCount,
      pageInfo {
      startCursor
      endCursor
    }
    edges {
      node {
        ... on Repository {
          name
          forkCount
          projectsUrl
          stargazerCount
          id
        }
      }
    }
  }
}
`;

const useStyles = makeStyles({
    container: {
        marginTop: 40,
    },
    table: {
        maxHeight: 600,
    }
});

function RepositoryListPage(){
    const [page, setPage] = React.useState(0);
    const endCursorRef = useRef(null);
    const classes = useStyles();
    const rowsPerPage = 20;

    const { loading, error, data , fetchMore} = useQuery(GET_REPOSITORY_LIST,{
        variables: { query: 'react', type: 'REPOSITORY', first: 20 , after: null},
        notifyOnNetworkStatusChange: true,
    });

    const repositoryLength = data?.search?.repositoryCount;
    endCursorRef.current = data?.search?.pageInfo?.endCursor;


    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
        fetchMore({
            variables: { query: 'react', type: 'REPOSITORY', first: 20, after: endCursorRef.current},
            updateQuery: (previousResult: QueryResultType, { fetchMoreResult }) => {
                const newEdges = fetchMoreResult.search.edges;
                const pageInfo = fetchMoreResult.search.pageInfo;
                const repositoryCount = fetchMoreResult.search.repositoryCount;
                endCursorRef.current = pageInfo?.endCursor;

                return  {
                    search: {
                        repositoryCount,
                        edges: [...previousResult.search.edges, ...newEdges],
                        pageInfo
                    }
                }
            }
        }).then((result)=>{
            console.info(result);
        });
    };


    if (loading ){
        return <Loading/>
    }

    if(error){
        return <OnError />
    }

    return (
        <Container className={classes.container}>
            <TableContainer className={classes.table} component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Stars</TableCell>
                            <TableCell>Forks</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.search.edges.map((repository: RepositoryListType) => {
                            return <RepositoryList key={repository.node.id} node={repository.node}/>
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                component="div"
                count={repositoryLength / 20}
                rowsPerPage={rowsPerPage}
                rowsPerPageOptions={[]}
                page={page}
                onPageChange={handleChangePage}
            />
        </Container>
    );
}

export default RepositoryListPage;
