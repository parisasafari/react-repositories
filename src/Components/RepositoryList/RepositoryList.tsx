import React from 'react';
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import {RepositoryListType} from '../../Types/Interface';

function RepositoryList({node}: RepositoryListType){
    return <TableRow>
        <TableCell>
            <a target="_blank" rel="noreferrer" href={node.projectsUrl}>
                {node.name}
            </a>
        </TableCell>
        <TableCell>{node.forkCount}</TableCell>
        <TableCell>{node.stargazerCount}</TableCell>
    </TableRow>
}

export default RepositoryList;
