import React from 'react';
import Container from "@material-ui/core/Container";
import CircularProgress from "@material-ui/core/CircularProgress";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
    loading: {
        marginTop: 80,
        justifyContent: 'center',
        width: '100%',
        display: 'flex',
    },
});


function Loading(){
    const classes = useStyles();

    return  <Container className={classes.loading}>
        <CircularProgress color="secondary" />
    </Container>
}

export default Loading;
