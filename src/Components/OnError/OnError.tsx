import React from 'react';
import {makeStyles} from "@material-ui/core";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Container from "@material-ui/core/Container";

const useStyles = makeStyles({
    container: {
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        marginTop: 100,
    },
    error: {
        width: 400,
        height: 400,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 14,
    },
});


function OnError(){
    const classes = useStyles();

    return  <Container className={classes.container}>
        <Card className={classes.error}>
        <CardContent>
            <Typography className={classes.title}>
                We Have An Error
            </Typography>
        </CardContent>
    </Card>
    </Container>
}

export default OnError;
