import React from 'react';
import RepositoryListPage from "./Components/RepositoryListPage/RepositoryListPage";

function App() {
  return (
    <RepositoryListPage />
  );
}

export default App;
